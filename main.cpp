﻿#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>
#include <pacman.hpp>
#include <iostream>
#include <food.hpp>

#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")


using namespace std::chrono_literals;

int main()
{

    float t = 0;
    float sx = 550;
    float sy = 430;

    sf::RenderWindow window(sf::VideoMode(800, 500), " PACMAN DEVOURER ");

    
    sf::Texture texture;
    if (!texture.loadFromFile("img/black.jpg"))
    {
        std::cout << "ERROR when loading back1.jpg" << std::endl;
        return false;
    }
    sf::Sprite back;
    back.setTexture(texture);

    
    
    sf::Image icon;
    if (!icon.loadFromFile("img/earth32.png"))
    {
        return -1;
    }
    window.setIcon(48, 48, icon.getPixelsPtr());
    
    
    std::vector<kr::Food*> foods;
    foods.push_back(new kr::Food(320, 200, 200));
    
  
    for (const auto& food : foods)
        if (!food->Setup())
            return -1;

     kr::Pacman* pacman = nullptr;

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }


        
        if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {   
            sf::Vector2i mp = sf::Mouse::getPosition(window);

            float ay = mp.y - sy;
            float ax = mp.x - sx;

            float angle = acos(ax / sqrt(ax * ax + ay * ay));

            if (pacman != nullptr)
                delete pacman;

            pacman = new kr::Pacman(sx, sy, 210, angle);


            if (!pacman->Setup())
            {
                delete pacman;
                window.close();
                return -1;
            }

            t = 0;
        }

        if (pacman != nullptr)
        {
            pacman->Move(t);
            
        }

        window.clear();
        window.draw(back);

        for (const auto& earth : foods)
            window.draw(*earth->Get());

 
        

        if(pacman != nullptr)
            window.draw(*pacman->Get());

        window.display();
        std::this_thread::sleep_for(40ms);
        t += 0.04;
    }

    if (pacman != nullptr)
        delete pacman;

    return 0;
}
