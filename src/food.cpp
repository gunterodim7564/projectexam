#include <food.hpp>

namespace kr//������������ ����
{
	Food::Food(int x, int y, float r)
	{
		m_x = x;
		m_y = y;
		m_r = r;
	}

	bool Food::Setup()
	{

		if (!m_texture.loadFromFile("img/food.png"))
		{
			std::cout << "ERROR when loading food.png" << std::endl;
			return false;
		}


		m_food = new sf::Sprite();
		m_food->setTexture(m_texture);
		m_food->setOrigin(m_r, m_r);
		m_food->setPosition(m_x, m_y);//������������� ����� � ������������ �����
		m_food->setScale(0.3, 0.3);

		return true;
	}

	Food::~Food()
	{
		if (m_food != nullptr)
			delete m_food;
	}

	sf::Sprite* Food::Get() { return m_food; }//����� get ����� �������� ���������� ������


	int Food::GetX() { return m_x; }
	int Food::GetY() { return m_y; }
	float Food::GetR() { return m_r; }
}
