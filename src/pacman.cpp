#include <pacman.hpp>
#include <iostream>

namespace kr
{
	
	Pacman::Pacman(int x0, int y0, float r, float angle)
	{
		m_x0 = x0;
		m_y0 = y0;
		m_r = r;
		m_angle = (2 * acos(-1) - angle);
		
	}

	bool Pacman::Setup()//��������� �������
	{
		if (!m_texture.loadFromFile("img/pacmanface.png"))
		{
			std::cout << "ERROR when loading moon1.png" << std::endl;
			return false;
		}

		m_pacman = new sf::Sprite();
		m_pacman->setTexture(m_texture);
		m_pacman->setOrigin(m_r, m_r);
		m_pacman->setPosition(m_x, m_y);

		return true;
	}

	Pacman::~Pacman()
	{
		if (m_pacman != nullptr)
			delete m_pacman;
	}

	void Pacman::Move(float t)
	{
		
		m_x = m_x0 + m_r * cos(m_angle * t/8);
		m_y = m_y0 + m_r * sin(m_angle * t/8);
		m_pacman->setPosition(m_x, m_y);
	}

	sf::Sprite* Pacman::Get() { return m_pacman; }//����� get ����� �������� ���������� ������

	int Pacman::GetX() { return m_x; }
	int Pacman::GetY() { return m_y; }
	float Pacman::GetR() { return m_r; }
}