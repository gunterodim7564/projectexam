#pragma once
#include <SFML/Graphics.hpp>

namespace kr
{
	

	class Pacman
	{
	public:
		
		Pacman(int x0, int y0, float r, float angle);
		~Pacman();

		bool Setup();
		void Move(float t);

		sf::Sprite* Get();
		int GetX();
		int GetY();
		float GetR();
	private:
		int m_x, m_y, m_x0, m_y0;
		float m_r;
		float m_angle;
		
		sf::Texture m_texture;
		sf::Sprite* m_pacman = nullptr;
	};

}