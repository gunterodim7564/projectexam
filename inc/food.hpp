#pragma once
#include <iostream>
#include <SFML/Graphics.hpp>

namespace kr
{
	class  Food
	{
	public:
		Food(int x, int y, float r);
		~Food();

		bool Setup();

		sf::Sprite* Get();
		int GetX();
		int GetY();
		float GetR();

	private:
		int m_x, m_y;
		float m_r;

		sf::Texture m_texture;
		sf::Sprite* m_food = nullptr;
	};
}

